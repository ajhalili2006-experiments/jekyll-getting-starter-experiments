# Jekyll Getting Started Tutorial Experiment

This repository is manually prepoulated, as per [the Jekyll tutorial from the Jekyll docs itself], in order of commits:

## Starter Pack Checklist

Since we're using GitLab CI + GitLab Pages instead of GitHub's counterparts, things might go few meters away from the tutorial road, so beware.

* Setting up Bundler + bare-bones Jekyll site
  * Step doc: <https://jekyllrb.com/docs/step-by-step/01-setup/>
  * Commit: <https://gitlab.com/ajhalili2006-experiments/jekyll-getting-starter-experiments/commit/cd13ca3f25a92d9c6087beb64adc02cb5a34d7bd>
* Liquid
  * Step doc: <https://jekyllrb.com/docs/step-by-step/02-liquid/>
  * Commit: <https://gitlab.com/ajhalili2006-experiments/jekyll-getting-starter-experiments/commit/d110b176200b3f0ad81b846de7de34c71fd8194e>
* Front Matter:
  * Step doc: <https://jekyllrb.com/docs/step-by-step/03-front-matter/>
  * Commit: <https://gitlab.com/ajhalili2006-experiments/jekyll-getting-starter-experiments/commit/b5889ae3a3376c0af116f1defd591d7a6237a86a>
* Layouts
  * Step doc: <https://jekyllrb.com/docs/step-by-step/04-layouts/>
  * Commit: <https://gitlab.com/ajhalili2006-experiments/jekyll-getting-starter-experiments/commit/404bf5cbb20296c14e2906fb1763021f1ac12cb5>, <https://gitlab.com/ajhalili2006-experiments/jekyll-getting-starter-experiments/commit/c8cc40ba96844ddb0a71577c17510efd1286aca2>
* Includes
  * Step doc: <https://jekyllrb.com/docs/step-by-step/05-includes/>
  * Commit: todo
* Data Files
  * Step doc: <https://jekyllrb.com/docs/step-by-step/06-data-files/>
  * Commit: todo

## License

MIT
